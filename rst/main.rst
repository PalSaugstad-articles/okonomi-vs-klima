.. raw:: html

   <!-- (php # 2 __DATE__ Økonomi vs. klima) -->
   <!-- ($attrib_AC=0;) -->

=================
Økonomi vs. klima
=================

*Av: |author| / |datetime|*

.. include:: revision-header.rst

Jeg diskuterer ofte med min nå 23 år gamle sønn, Are.
I går var temaet økonomi og klima, i hvilken rekkefølge må de fikses?
Jeg mente at folk legger altfor mye vekt på klimatiltak, og glemmer at økonomisk teori ikke klarer å ta inn over seg dagens problemer.
Are mente at det å få ned CO2-utslipp er det aller viktigste.
Vi var enige om at begge deler er viktige, men Are mente at hvis man må prioritere, så må man se på klimatiltak først,
for så å rydde opp i eventuelle økonomiske problemer senere.
Jeg mente omvendt, at hvis vi må velge, så må vi prioritere økonomi først.
Vi ble vel etterhvert enige i at det ikke er snakk om å velge, vi må gjøre begge deler samtidig.
For hvis vi kun fokuserer på klima, så kan vi risikere at økonomien går over styr med økt arbeidsledighet
(som følge av at folk blir tilbakeholdende med luksusforbruk som lange flyturer og liknende),
som i sin tur kan føre til opptøyer á la 'gule vester' og konflikter på flere hold.
Hvis man bare fokuserer på økonomiske problemer som f. eks. økende ulikhet, så kan det tenkes at det vil ta for lang tid
før vi får klimaproblene under kontroll.
Hvis jorda blir ubeboelig, så spiller det liten rolle om økonomien fungerer.

Hvordan i all verden skal man klare å gjøre noe med begge deler samtidig? Grønn vekst er foreslått av mange.
Det er en allmenn forståelse for at ulikhet ikke kan bekjempes uten økonomisk vekst.
Dessuten trengs omfattende tiltak for å fase ut fossil-industrien, så da er vel saken enkel?

Kan vi ha evig grønn vekst? Neppe. Før eller siden kommer anti-vekst-spøkelset etter oss.
Antakelig er det over oss allerede, helt uavhengig av klima-problemer.
I de mest velstående landene har det vært svært lav vekst i det siste.
Det synes å være tvingende nødvendig å finne måter å minske ulikhet på uten vekst.
Under og etter finanskrisen i 2008 fikk vi en reduksjon i klima-utslipp.
Det å slappe av, ikke gjøre stort, kan føre til konkurser og mye elendighet, men det vil også føre til en begrensning av klima-utslipp.

Teoretisk sett vil en sammentrekning av økonomien samtidig som at vi fokuserer på å erstatte fossilt energiforbruk med
fornybart være en bra kombinasjon.

Hva så med ulikhet og urettferdighet?
Bør vi vurdere borgerlønn og 'karbonskatt til fordeling'?
Jeg tror at vi også bør sjekke ganske grundig hvordan bank- og finans-systemene fungerer.
Vi ser en dreining mot individuell pensjons-sparing.
Er dette riktig vei å gå?
Oppbygging av formue i fond har noen uønskede bivirkninger.
Det forsterker ulikhet fordi det er først og fremst de som allerede er velstående som har mulighet til å gjøre seg enda rikere via fond.

Det øker også indirekte gjeldsgraden fordi kjøperne av fond bytter ut noe av pengene sine med verdipapier og obligasjoner.
Dermed forsvinner penger fra bedrifter og husholdninger, uten at noe gjeld forsvinner.
Da må mer penger lånes ut av bankene for å opprettholde pengemengden, en prosess som øker pengemengden og gjeldsmengden like mye målt i kroner.
Slik sett fungerer kombinasjonen av bank og finans som en gigantisk ulikhetskværn.

Sparing i fond gjør oss også helt avhengige av økonomisk vekst.
Uten vekst vil verdiene i fondene kunne fordufte dersom troen på finansmarkedene fordufter, og som kjent, investorer er flokkdyr.
Hvis noen begynner å miste troen tar det ikke lang tid før vi ser et skikkelig ras.

Hva må vi gjøre med dette?
Antakelig er det ingen vei utenom en ny finanskrise som er minst like dyp som den vi hadde i 2008.
Vi kunne nesten like gjerne framprovosere en kollaps ved å innføre Tobin-skatt, altså en skatt på finanstransaksjoner.
Bankvesenet bør bli frakoblet finansvesenet i tilstrekkelig grad til at banksystemet ikke går nedenom samtidig.
Én mulighet her er at sentralbanken oppretter elektroniske kontanter, slik at alle kan ha konto direkte i sentralbanken.

Det er på tide at folk med økonomisk innsikt ser med nye øyne på vår tids problemer. Vi ser noe bevegelse,
professor i samfunnsøkonomi Kalle Moene skrev en tankevekkende kronikk i Dagens Næringsliv tidligere i år:
"`Det falske løftet om økonomisk vekst
<https://www.sv.uio.no/econ/om/aktuelt/i-media/2019/2019-03-15-moene.html>`_".
Her tar han opp sammenhengen mellom
vareproduksjon og skadelige utslipp, og at større rikdom for et samfunn ikke nødvendigvis fører til større lykke.

Professor i samfunnsøkonomi Arne Jon Isachsen har skrevet
"`Om å sette pris på utslipp
<https://www.nettavisen.no/nyheter/om-a-sette-pris-pa-utslipp/3423661340.html>`_"
i Nettavisen, der han belyser nødvendigheten av å avgiftsbelegge CO2-utslipp.
I Aftenposten skrev han
"`Oljefondet: Fortjener vi alle disse pengene?
<https://www.aftenposten.no/meninger/debatt/i/Wb0VJr/Oljefondet-er-penger-tjent-pa-bekostning-av-noe-Bor-vi-opprette-Statens-fond-til-bekjempelse-av-global-oppvarming--Arne-Jon-Isachsen>`_".
Her lufter han tanken om å lage et nytt fond,
Statens fond til bekjempelse av global oppvarming.

I vår tid med nye problemer som verden ikke har sett før, oppstår det også nye tanker som få har tenkt før.
Slike tanker må tas på alvor.
Min generasjon, vi som er over 50 år, må bidra i denne debatten.
Oppfatningen om at vi må satse på det som gir størst avkastning og la eierene stable opp formue i fond og obligasjoner må utfordres.
Det at industri som lønner seg mest på kort sikt også påvirker naturmangfoldet i negativ retning må belyses.

Vi har sett at yngre generasjoner, som min sønn Are og enda yngre,
begynner å stille betimelige spørsmål om hvilken retning de ønsker at samfunnet skal gå i.
De er i sin fulle rett til å kreve svar av nåværende generasjon samfunnstopper og politikere.

.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Økonomi vs. klima">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/okonomi-vs-klima/nor/okonomi-vs-klima.pdf">som pdf</a>
